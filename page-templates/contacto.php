<?php /* Template Name: Contacto */ ?>

<?php get_header(); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>
		<?php $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>
		<!--Imagen Logo-->
		<div class="container-fluid white space3">
			<div class="row">
				<div class="space20"></div>
				<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
					<div class="contenedor-contacto" style="position: relative;">
						<img class="responsive-img-full-w-h" src="<?php echo $image; ?>">
						<div class="contenedor-logo-contacto">
							<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/Logo_2.png">
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Contacto-->
		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<div class="col s12 m4 l4">
						<div class="space40 hide-on-small-only"></div>
						<h3 class="bold font48 gris mobile-text-center uppercase tablet-font24">nosotros</h3>
					</div>
					<div class="col s12 m8 l8">
						<p class="light font16 gris mobile-text-center">
							<?php the_content(); ?>	
						</p>
						<div class="space30"></div>
						<p class="bold font16 mobile-text-center">
							<i class="fa fa-phone" aria-hidden="true" style="margin-right: 15px;"></i><?php the_field("phone", $post->ID); ?>
						</p>
						<p class="bold font16 mobile-text-center">
							<i class="fa fa-envelope-o" aria-hidden="true" style="margin-right: 10px;"></i><?php the_field("email", $post->ID); ?>
						</p>
					</div>
				</div>
			</div>
		</div>

		<!--GPS Ubicación-->
		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
				<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
					<?php 
		
						$location = get_field('location');
						
						if( !empty($location) ):
					?>
					<div class="acf-map">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<!--Contacto Formulario-->
		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<div class="space40 hide-on-small-only"></div>
					<h3 class="bold font48 gris mobile-text-center uppercase tablet-font24">escribenos</h3>
				</div>
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<form class="col s12 formulario-editar" id="formContact" name="formContact" style="padding: 0;">
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Nombre" id="name" name="name" type="text" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Correo Electrónico" id="email" name="email" type="email" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Teléfono" id="phone" name="phone" type="tel" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<!--<div class="row">
							<form action="#">
								<p class="light font16">
									Suscribete a nuestro Newsletter   
									<input type="checkbox" class="filled-in" id="filled-in-box" />
									<label for="filled-in-box" style="color: transparent; margin-left: 10px;">n</label>
								</p>
							</form>
						</div>-->
						<div class="row">
							<form class="col s12" style="padding: 0;">
								<div class="row">
									<div class="input-field col s12" style="padding: 0; margin: 0;">
										<textarea placeholder="Comentarios" id="comment" name="comment" class="edicion-textarea-form materialize-textarea"></textarea>
									</div>
								</div>
							</form>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="btn-enviar centered">
									<button type="submit" id="btnSendContact" name="btnSendContact" class="btn-enviar-formulario browser-default gris bold font20 uppercase">Enviar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?php endwhile; ?>
		<?php endif; ?>

<?php get_footer(); ?>