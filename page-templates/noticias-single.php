<?php /* Template Name: Noticias-Single */ ?>

<?php get_header(); ?>

<div class="container-fluid" style="background-color: #FFF">
	<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
		<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
			<div class="contenedor-img-single">
				<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/img_noticias_single.png">
			</div>
		</div>
		<div class="col s12 m8 offset-m2 l8 offset-l2">
			<div class="space40"></div>
			<h3 class="bold font34 centered uppercase">Juan Hernandez Y Su Banda De Blues</h3>
			<div class="space40"></div>
			<h4 class="helvetica light font18 centered uppercase">SEPTIEMBRE 18 - 2017</h4>
			<div class="space40"></div>
			<p class="light font16 centered mobile-text-justify" style="max-width: 850px;">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nec pharetra lacus, eget maximus odio. Phasellus luctus, justo faucibus sagittis fringilla, nibh tellus semper leo, vel aliquet augue arcu non purus. Praesent posuere dui ac imperdiet imperdiet. Donec at augue rhoncus quam vehicula facilisis. Vestibulum felis ex, scelerisque eget risus id, aliquam sollicitudin sem. Nunc dignissim nisi in est elementum, bibendum pretium sem pellentesque. Donec sed ultricies dui. Morbi consequat, mi vel condimentum sollicitudin, ipsum elit venenatis dui, quis dignissim justo ante venenatis turpis. Ut hendrerit quis nulla eget ultrices. Praesent lectus ipsum, tincidunt a nunc eu, fringilla pharetra nunc.
			</p>
			<div class="space40"></div>
		</div>
		<div class="col s12">
			<h3 class="bold font18 centered uppercase">COMPARTIR</h3>
			<div class="space20"></div>
			<div class="contenedor-face block centered">
				<a href="#" class="bold font20 white-text uppercase btn-facebook-sinlge inline">facebook</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid" style="background-color: #E8E8E8;">
	<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
		<div class="col s12 m10 offset-m1 l10 offset-l1">
			<div class="space40"></div>
			<h3 class="bold font34 gris uppercase mobile-text-center">más noticias</h3>
			<div class="space40"></div>
		</div>
		<div class="col s12 m10 offset-m1 l10 offset-l1 no-padding">
			<div class="col s12 m4 l4 mobile-padding-0">
				<div class="contenedor-ficha-noticias">
					<div class="card grayscale">
						<a href="http://dev.titanio.com/noticias-single/" style="color: inherit;">
							<div class="card-image">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/img_noticias.png">
							</div>
							<div class="card-content">
								<h3 class="bold font14 mobile-text-center titulo-ficha">Juan Hernandez Y Su Banda De Blues</h3>
								<p class="font12 mobile-text-center contenido-ficha">Presenta uno de los temas mas escuchados del album al rojo vivo.</p>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="col s12 m4 l4 mobile-padding-0">
				<div class="contenedor-ficha-noticias">
					<div class="card grayscale">
						<a href="http://dev.titanio.com/noticias-single/" style="color: inherit;">
							<div class="card-image">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/img_noticias.png">
							</div>
							<div class="card-content">
								<h3 class="bold font14 mobile-text-center titulo-ficha">Juan Hernandez Y Su Banda De Blues</h3>
								<p class="font12 mobile-text-center contenido-ficha">Presenta uno de los temas mas escuchados del album al rojo vivo.</p>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="col s12 m4 l4 mobile-padding-0">
				<div class="contenedor-ficha-noticias">
					<div class="card grayscale">
						<a href="http://dev.titanio.com/noticias-single/" style="color: inherit;">
							<div class="card-image">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/img_noticias.png">
							</div>
							<div class="card-content">
								<h3 class="bold font14 mobile-text-center titulo-ficha">Juan Hernandez Y Su Banda De Blues</h3>
								<p class="font12 mobile-text-center contenido-ficha">Presenta uno de los temas mas escuchados del album al rojo vivo.</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>