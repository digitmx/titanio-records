<?php /* Template Name: Servicios */ ?>

<?php get_header(); ?>

		<?php 
			
			//Consultamos las noticias
			$args = array(
				'posts_per_page'   => -1,
				'order'			   => 'date',
				'orderby'          => 'DESC',
				'post_type'        => 'servicio',
				'post_status'      => 'publish',
				'suppress_filters' => false 
			);
			$posts_array = new WP_Query( $args ); 
			$count_services = 0;
			
		?>

		<!--Servicios-->
		<div class="container-fluid white">
			<div class="row">
				<div class="space20"></div>
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<h3 class="bold font48 gris uppercase mobile-text-center">servicios</h3>
				</div>
				<?php foreach ($posts_array->posts as $servicio) { $count_services++; $par = $count_services % 2; $cover = get_the_post_thumbnail_url( $servicio->ID, $size = 'full' ); ?>
				<div class="<?=($par) ? 'col s12 m5 offset-m1 l5 offset-l1 mobile-padding-0' : 'col s12 m5 l5 mobile-padding-0'; ?>">
					<div class="space20"></div>
					<div class="contenedor-imagen-servicios">
						<img class="responsive-img-full-w-h grayscale" src="<?php echo $cover; ?>">
					</div>
					<div class="space20"></div>
					<div class="bold font15 block mobile-text-center">
						<p class="black white-text uppercase inline" style="padding: 10px 35px;">
							<?php echo $servicio->post_title; ?>
						</p>
					</div>
					<div class="bold font18 block mobile-text-center">
						<p class="uppercase">
							<?php the_field("subtitle", $servicio->ID); ?>
						</p>
					</div>
					<div class="light block font16 mobile-text-center text-justify">
						<p class="text-servicio">
							<?php echo $servicio->post_excerpt; ?>
						</p>
					</div>
					<div class="space20"></div>
					<div class="btn-cotizar mobile-text-center">
						<a href="<?php echo get_permalink($servicio->ID); ?>" class="bold inline font20 boton-cotizar gris">COTIZAR</a>
					</div>
					<div class="space20"></div>
				</div>
				<?php wp_reset_postdata(); } ?>
			</div>
		</div>

<?php get_footer(); ?>