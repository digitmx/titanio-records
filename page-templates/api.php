<?php /* Template Name: API */

	//Include PHPMailer
	include_once( getcwd().'/wp-includes/class-phpmailer.php' );
	
	//Read Param Field
	$json = (isset($_POST['param'])) ? $_POST['param'] : NULL; $output = FALSE;
	$json = str_replace('\\', '', $json);

	//Check JSON
	if ($json != NULL)
	{
		//Decode Data JSON
		$json_decode = json_decode($json, true);

		//Read Action JSON
		$msg = (isset($json_decode['msg'])) ? (string)trim($json_decode['msg']) : '';

		//Read Fields JSON
		$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : array();
		
		//demo
		if ($msg == 'demo')
		{
			//Mail Notification
			$subject = 'Nueva cotización de Titanio Records';
			$template = '<html><body>';
			$template.= 'Nombre: Emiliano<br/>';
			$template.= 'Email: milio.hernandez@gmail.com<br/>';
			$template.= 'Telefono: 5519522071<br/>';
			$template.= 'Servicio: Digital<br/>';
			$template.= 'Comentario: Prueba<br/>';
			$template.= '</body></html>';
			$body = $template;
			$headers = array('Content-Type: text/html; charset=UTF-8');
			$altbody = strip_tags($body);
			$status = 'fail';

			//Check Email Data
			if ($subject != '' && $body != '' && $altbody != '')
			{
				//Send Email
				$response = wp_mail( 'milio.hernandez@gmail.com', $subject, $body, $headers );
				if ($response) { $status = 'send'; }
			}
			
			//Show Results
			$array = array(
				'status' => (int)1,
				'msg' => (string)'success',
				'data' => $status
			);
			
			//Print JSON Array
			printJSON($array);
			$output = TRUE;
		}
		
		//sendBudget
		if ($msg == 'sendBudget')
		{
			//Leemos los campos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$phone = (isset($fields['phone'])) ? (string)trim($fields['phone']) : '';
			$service = (isset($fields['service'])) ? (string)trim($fields['service']) : '';
			$comment = (isset($fields['comment'])) ? (string)trim($fields['comment']) : '';
			$notification = get_field("email_notification", "option");
			
			//Verificamos los Campos
			if ($name && $email && $phone && $service && $comment)
			{
				//Mail Notification
				$subject = 'Nueva cotización de Titanio Records';
				$template = '<html><body>';
				$template.= 'Nombre: ' . $name . '<br/>';
				$template.= 'Email: ' . $email . '<br/>';
				$template.= 'Telefono: ' . $phone . '<br/>';
				$template.= 'Servicio: ' . $service . '<br/>';
				$template.= 'Comentario: ' . $comment . '<br/>';
				$template.= '</body></html>';
				$body = $template;
				$headers = array('Content-Type: text/html; charset=UTF-8');
				$altbody = strip_tags($body);

				//Check Email Data
				if ($subject != '' && $body != '' && $altbody != '')
				{
					//Send Email
					$response = wp_mail( $notification, $subject, $body, $headers );
				}
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success'
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//sendContact
		if ($msg == 'sendContact')
		{
			//Leemos los campos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$phone = (isset($fields['phone'])) ? (string)trim($fields['phone']) : '';
			$comment = (isset($fields['comment'])) ? (string)trim($fields['comment']) : '';
			$notification = get_field("email_notification", "option");
			
			//Verificamos los Campos
			if ($name && $email && $phone && $comment)
			{
				//Mail Notification
				$subject = 'Nuevo contacto en Titanio Records';
				$template = '<html><body>';
				$template.= 'Nombre: ' . $name . '<br/>';
				$template.= 'Email: ' . $email . '<br/>';
				$template.= 'Telefono: ' . $phone . '<br/>';
				$template.= 'Comentario: ' . $comment . '<br/>';
				$template.= '</body></html>';
				$body = $template;
				$headers = array('Content-Type: text/html; charset=UTF-8');
				$altbody = strip_tags($body);

				//Check Email Data
				if ($subject != '' && $body != '' && $altbody != '')
				{
					//Send Email
					$response = wp_mail( $notification, $subject, $body, $headers );
				}
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success'
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
	}
	else
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Call Invalid.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}

	//Check Output
	if (!$output)
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Error.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}
	
?>