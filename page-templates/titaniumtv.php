<?php /* Template Name: Titanium TV */ ?>

<?php get_header(); ?>

		<?php $videos = get_field("videos_titaniotv", "option"); ?>
		<div class="container-fluid space3 white">
			<div class="row">
				<div class="col s12 hide-on-med-and-up">
					<div class="space40"></div>
					<h3 class="bold font24 gris uppercase centered">TITANIO TV</h3>
					<div class="space20"></div>
					<div class="contenedor-suscribirse-mobile centered">
						<a href="<?php the_field("url_youtube","option"); ?>" target="_blank" class="bold font16 gris uppercase">suscribirse</a>
					</div>
				</div>
				<?php $video_principal = $videos[0]; ?>
				<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
					<div class="space40"></div>
					<div class="col s12 m12 l7 mobile-padding-0" style="margin-bottom: 40px;">
						<a href="#" class="">
							<div class="video-container">
								<?php echo $video_principal['iframe']; ?>
							</div>
							<!--<div class="contenedor-video-principal grayscale">
								<img class="responsive-img-full-w-h" src="../wp-content/themes/titanio_v1/img/img_titaniotv_slider.png">
							</div>-->
						</a>
						<div class="space10"></div>
						<div class="row" style="margin-bottom: 0;">
							<div class="col s12 m8 l9">
								<h3 class="edit-titulo-tv bold font24 mobile-text-center uppercase mobile-padding-10"><?php echo $video_principal['title']; ?></h3>
							</div>
							<div class="col s12 m4 l3 hide-on-small-only" style="padding-right: 0; padding-left: 0;">
								<div class="contenedor-suscribirse" style="line-height: 70px;">
									<a href="<?php the_field("url_youtube","option"); ?>" target="_blank" class="bold font16 gris uppercase">suscribirse</a>
								</div>
							</div>
						</div>
						<p class="edit-parrafo-tv light font16 gris mobile-text-justify mobile-padding-10"><?php echo $video_principal['description']; ?></p>
					</div>
					<?php $banners = get_field("banners_titaniotv", "option"); ?>
					<div class="col s12 m12 l5 mobile-padding-0 banner_titanio">
						<?php foreach($banners as $banner) { ?>
						<div class="banner_content">
						    <a href="<?php echo $banner['link']; ?>" class="">
								<div class="contenedor-banner-evento">
									<img class="responsive-img centered" src="<?php echo $banner['image']; ?>">
								</div>
							</a>
							<div class="space10"></div>
							<div class="row" style="margin-bottom: 0;">
								<div class="col s12 m12 l12">
									<h3 class="edit-titulo-tv bold font24 mobile-text-center uppercase mobile-padding-10"><?php echo $banner['title']; ?></h3>
								</div>
							</div>
							<p class="edit-parrafo-tv light font16 gris mobile-text-justify mobile-padding-10"><?php echo $banner['description']; ?></p>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 10px;">
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<h3 class="bold font34 gris uppercase mobile-text-center">más videos</h3>
					<div class="space40"></div>
				</div>
				<div class="col s12 m10 offset-m1 l10 offset-l1 no-padding">
					<?php $contador_videos = 0; ?>
					<?php foreach ($videos as $video) { $contador_videos++; ?>
						<?php if ($contador_videos > 1) { ?>
						<div class="col s12 m4 l4 mobile-padding-0">
							<div class="video-container">
								<?php echo $video['iframe']; ?>
							</div>
							<div class="row">
								<div class="col s12 no-padding">
									<h3 class="titulo-mas-videos bold font16 mobile-text-center uppercase"><?php echo $video['title']; ?></h3>
								</div>
							</div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>

<?php get_footer(); ?>