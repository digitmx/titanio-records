<?php get_header(); ?>

		<!--Slider-->
		<?php $sliders = get_field("sliders_home","option"); ?>
		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row mobil-margin-bottom-0">
				<div class="space40 hide-on-small-only"></div>
				<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
					<div class="slider">
						<ul class="slides" style="position: relative;">
							<?php foreach ($sliders as $slider) { ?>
							<li>
								<a href="<?php echo $slider['link']; ?>">
									<img src="<?php echo $slider['image']; ?>">
									<div class="caption left-align">
										<h3 class="bold font30 uppercase"><?php echo $slider['title']; ?></h3>
										<h5 class="flow-text light grey-text text-lighten-3 text-justify mobile-text-center"><?php echo $slider['text']; ?></h5>
									</div>
								</a>
							</li>
							<?php } ?>
							<a href="#" class="btn-back hide-on-small-only">
								<img class="z-depth-5-shadow-back" src="<?php bloginfo('template_directory'); ?>/img/btn-back.png">
							</a>
							<a href="#" class="btn-forward hide-on-small-only">
								<img class="z-depth-5-shadow-forward" src="<?php bloginfo('template_directory'); ?>/img/btn-forward.png">
							</a>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!--Banner-->
		<?php $banner_slider = get_field("banner_slider","option"); foreach ($banner_slider as $banner) { } ?>
		<div class="container-fluid no-margin-row" style="background-color: #FFF;">
			<div class="container" style="background-color: #FFF;">
				<div class="row">
					<div class="col s12 m10 offset-m1 centered">
						<a href="<?php echo $banner['link']; ?>">
							<img class="responsive-img" src="<?php echo $banner['image']; ?>">
						</a>
					</div>
				</div>
			</div>
		</div>

		<!--Servicios-->
		<?php $services = get_field("services_home","option"); $count_services = 0; ?>
		<div class="container-fluid" style="background-color: #FFF; margin-top: -20px;">
			<div class="row">
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<h3 class="bold font48 gris uppercase mobile-text-center">servicios</h3>
				</div>
				
				<?php foreach ($services as $service) { $count_services++; $par = $count_services % 2; ?>
				<div class="<?=($par) ? 'col s12 m5 offset-m1 l5 offset-l1 mobile-padding-0' : 'col s12 m5 l5 mobile-padding-0'; ?>">
					<div class="space20"></div>
					<div class="contenedor-imagen-servicios">
						<img class="responsive-img-full-w-h grayscale" src="<?php echo get_the_post_thumbnail_url( $service->ID, $size = 'full' ); ?>">
					</div>
					<div class="space20"></div>
					<div class="bold font15 block mobile-text-center">
						<p class="black white-text uppercase inline" style="padding: 10px 35px;">
							<?php echo $service->post_title; ?>
						</p>
					</div>
					<div class="bold font18 block mobile-text-center">
						<p class="uppercase">
							<?php echo get_field('subtitle', $service->ID); ?>
						</p>
					</div>
					<div class="light block font16 mobile-text-center text-justify">
						<p class="text-servicio">
							<?php echo $service->post_excerpt; ?>
						</p>
					</div>
					<div class="space20"></div>
					<div class="btn-cotizar mobile-text-center">
						<a href="<?php the_permalink($service->ID); ?>" class="bold inline font20 boton-cotizar gris">COTIZAR</a>
					</div>
					<div class="space20"></div>
				</div>
				<?php } ?>
				
			</div>
		</div>
		
		<!--Banner-->
		<?php $banner_services = get_field("banner_services","option"); foreach ($banner_services as $banner) { } ?>
		<div class="container-fluid no-margin-row" style="background-color: #FFF; padding: 20px 0;">
			<div class="container" style="background-color: #FFF;">
				<div class="row">
					<div class="col s12 m10 offset-m1 centered">
						<a href="<?php echo $banner['link']; ?>">
							<img class="responsive-img" src="<?php echo $banner['image']; ?>">
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<!--Ultimos Videos-->
		<?php $videos = get_field("videos_titaniotv", "option"); ?>
		<?php
			//Leemos los Videos
			$video_one = '';
			$video_two = '';
			$video_three = '';	
			$contador = 0;
			
			foreach ($videos as $video)
			{
				$contador++;
				
				if ($contador == 1) { $video_one = $video['iframe']; }
				if ($contador == 2) { $video_two = $video['iframe']; }
				if ($contador == 3) { $video_three = $video['iframe']; }
			}
		?>
		<div class="container-fluid no-margin-row" style="background-color: #EDEDED;">
			<div class="row">
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<div class="space40"></div>
					<h3 class="bold font48 gris uppercase mobile-text-center">ÚLTIMOS VIDEOS</h3>
					<div class="space40"></div>
				</div>
				<div class="col s12 m7 offset-m1 l7 offset-l1">
					<div class="video-container">
						<?php echo $video_one; ?>
					</div>
					<div class="space40"></div>
				</div>
				<div class="col s12 m3 l3">
					<div class="video-container">
						<?php echo $video_two; ?>
					</div>
					<div class="space40"></div>
					<div class="video-container">
						<?php echo $video_three; ?>
					</div>
				</div>
				<div class="col s12 m7 offset-m1 l7 offset-l1">
					<div class="space80 hide-on-small-only"></div>
					<div class="space40 show-on-small"></div>
				</div>
			</div>
		</div>
		
		<!--Spotify-->
		<div class="container-fluid no-margin-row spoti-pos-columna" style="background-color: #FFF;">
			<div class="row">
				<div class="col s12 m12 l3 offset-l1">
					<div class="contenedor-titulo-seccion valign-wrapper">
						<h3 class="bold font48 gris uppercase mobile-text-center">lo mejor en SPOTIFY</h3>
					</div>
				</div>
				<div class="col s12 m6 l4">
					<div class="centered">
						<iframe src="https://open.spotify.com/embed/user/titanio-records/playlist/<?php echo get_field("playlist_spotify","option"); ?>" width="300" height="600" frameborder="0" allowtransparency="true"></iframe>
					</div>
				</div>
				<?php $banner_spotify = get_field("banner_spotify","option"); $banner_footer = array(); if ($banner_spotify) { foreach ($banner_spotify as $banner_footer) { } } ?>
				<div class="col s12 m6 l3 mobile-padding-0">
					<div class="contenedor-banner">
						<div class="img-banner300x600">
							<a href="<?php echo $banner_footer['link']; ?>">
								<img class="" src="<?php echo $banner_footer['image']; ?>"><!--Imagen de Banner-->
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>