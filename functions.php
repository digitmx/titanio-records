<?php

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page();

	}

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Titanio Records</i>';
	}
	function my_footer_version() {
	    return 'Version 1.1';
	}

	// Páginas de Configuración
	add_filter('acf/options_page/settings', 'my_options_page_settings');

	function my_options_page_settings ( $options )
	{
		$options['title'] = __('Administración del Tema');
		$options['pages'] = array(
			__('Home')
		);

		return $options;
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load CSS
		wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), '1.0.35' );
		
		// Load JS
		wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g', array(), '3', true );
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'app.js', array('gmaps'), '1.0.16', true );
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );
	
	function my_acf_google_map_api( $api ){
		$api['key'] = 'AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g';	
		return $api;
		
	}
	add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	
	function codex_custom_init() {

		//Servicios
		$labels = array(
		    'name' => _x('Servicios', 'post type general name'),
		    'singular_name' => _x('Servicio', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'New'),
		    'add_new_item' => __('Agregar Nuevo Servicio'),
		    'edit_item' => __('Editar Servicio'),
		    'new_item' => __('Nuevo Servicio'),
		    'all_items' => __('Todos los Servicios'),
		    'view_item' => __('Ver Servicio'),
		    'search_items' => __('Buscar Servicios'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Servicios'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-cart',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' )
		);
		register_post_type('servicio',$args);
		
		//Catalogo
		$labels = array(
		    'name' => _x('Catálogo', 'post type general name'),
		    'singular_name' => _x('Catálogo', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'New'),
		    'add_new_item' => __('Agregar Nuevo Elemento'),
		    'edit_item' => __('Editar Elemento'),
		    'new_item' => __('Nuevo Elemento'),
		    'all_items' => __('Todos los Elementos'),
		    'view_item' => __('Ver Elemento'),
		    'search_items' => __('Buscar Elementos'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Catálogo'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-format-audio',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' )
		);
		register_post_type('producto',$args);
		
		//Noticias
		$labels = array(
		    'name' => _x('Noticias', 'post type general name'),
		    'singular_name' => _x('Noticia', 'post type singular name'),
		    'add_new' => _x('Agregar Nueva', 'New'),
		    'add_new_item' => __('Agregar Nueva Noticia'),
		    'edit_item' => __('Editar Noticia'),
		    'new_item' => __('Nueva Noticia'),
		    'all_items' => __('Todas las Noticias'),
		    'view_item' => __('Ver Noticias'),
		    'search_items' => __('Buscar Noticias'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Noticias'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-welcome-write-blog',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' )
		);
		register_post_type('noticia',$args);
		
		//Grupo
		$labels = array(
		    'name' => _x('Grupos', 'post type general name'),
		    'singular_name' => _x('Grupo', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'Grupo'),
		    'add_new_item' => __('Agregar Nuevo Grupo'),
		    'edit_item' => __('Editar Grupo'),
		    'new_item' => __('Nuevo Grupo'),
		    'all_items' => __('Todos los Grupos'),
		    'view_item' => __('Ver Grupos'),
		    'search_items' => __('Buscar Grupos'),
		    'not_found' =>  __('No encontrado'),
		    'not_found_in_trash' => __('No encontrado en Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Grupos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-groups',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' )
		);
		register_post_type('grupo',$args);
	}
	
	//Genero
	add_action( 'init', 'categoria', 0 );
	
	function categoria() {
		$labels = array(
	    	'name' => __( 'Categoría' ),
			'singular_name' => __( 'Categoría' ),
			'search_items' =>  __( 'Buscar Categoría' ),
			'all_items' => __( 'Todas las Categorías' ),
			'parent_item' => __( 'Parent Categoría' ),
			'parent_item_colon' => __( 'Parent Categoría:' ),
			'edit_item' => __( 'Editar Categoría' ), 
			'update_item' => __( 'Actualizar Categoría' ),
			'add_new_item' => __( 'Agregar Categoría' ),
			'new_item_name' => __( 'Nombre Nueva Categoría' ),
			'menu_name' => __( 'Categorías' ),
		); 	
	
		register_taxonomy('categoria',array('producto'), array(
	    	'hierarchical' => true,
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'sort' => true,
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => 'categoria' ),
		));
	}
	
	//Iniciales
	add_action( 'init', 'inicial', 0 );
	
	function inicial() {
		$labels = array(
	    	'name' => __( 'Iniciales' ),
			'singular_name' => __( 'Inicial' ),
			'search_items' =>  __( 'Buscar Inicial' ),
			'all_items' => __( 'Todas las Iniciales' ),
			'parent_item' => __( 'Parent Inicial' ),
			'parent_item_colon' => __( 'Parent Inicial:' ),
			'edit_item' => __( 'Editar Inicial' ), 
			'update_item' => __( 'Actualizar Inicial' ),
			'add_new_item' => __( 'Agregar Inicial' ),
			'new_item_name' => __( 'Nombre Nueva Inicial' ),
			'menu_name' => __( 'Iniciales' ),
		); 	
	
		register_taxonomy('inicial',array('grupo'), array(
	    	'hierarchical' => true,
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'sort' => true,
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => 'inicial' ),
		));
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}

	//SUBIR IMAGEN A CAMPO IMG DE ACF
	function my_update_attachment($f,$pid,$t='',$c='') {
	  	wp_update_attachment_metadata( $pid, $f );
	  	if( !empty( $_FILES[$f]['name'] )) { //New upload
	    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
			include( ABSPATH . 'wp-admin/includes/image.php' );
			// $override['action'] = 'editpost';
			$override['test_form'] = false;
			$file = wp_handle_upload( $_FILES[$f], $override );

			if ( isset( $file['error'] )) {
				return new WP_Error( 'upload_error', $file['error'] );
	    	}

			$file_type = wp_check_filetype($_FILES[$f]['name'], array(
				'jpg|jpeg' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
			));

			if ($file_type['type']) {
				$name_parts = pathinfo( $file['file'] );
				$name = $file['filename'];
				$type = $file['type'];
				$title = $t ? $t : $name;
				$content = $c;

				$attachment = array(
					'post_title' => $title,
					'post_type' => 'attachment',
					'post_content' => $content,
					'post_parent' => $pid,
					'post_mime_type' => $type,
					'guid' => $file['url'],
				);

				foreach( get_intermediate_image_sizes() as $s ) {
					$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
					$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
					$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
					$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	      		}

		  		$sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

		  		foreach( $sizes as $size => $size_data ) {
		  			$resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
		  			if ( $resized )
		  				$metadata['sizes'][$size] = $resized;
	      		}

		  		$attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

		  		if ( !is_wp_error( $attach_id )) {
		  			$attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
		  			wp_update_attachment_metadata( $attach_id, $attach_meta );
	      		}

		  		return array(
		  			'pid' =>$pid,
		  			'url' =>$file['url'],
		  			'file'=>$file,
		  			'attach_id'=>$attach_id
		  		);
	    	}
	  	}
	}
	
	add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
	add_filter('previous_posts_link_attributes', 'posts_link_attributes_back');
	
	function posts_link_attributes_back() {
	    return 'class="bt-bk z-depth-5-shadow-forward"';
	}
	
	function posts_link_attributes_next() {
		return 'class="bt-fr z-depth-5-shadow-forward"';
	}

?>