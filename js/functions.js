
	(function($) {

	/*
	*  new_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/
	
	function new_map( $el ) {
		
		// var
		var $markers = $el.find('.marker');
		
		
		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};
		
		
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		
		
		// add a markers reference
		map.markers = [];
		
		
		// add markers
		$markers.each(function(){
			
	    	add_marker( $(this), map );
			
		});
		
		
		// center map
		center_map( map );
		
		
		// return
		return map;
		
	}
	
	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/
	
	function add_marker( $marker, map ) {
	
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
	
		// add to array
		map.markers.push( marker );
	
		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});
	
			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
	
				infowindow.open( map, marker );
	
			});
		}
	
	}
	
	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/
	
	function center_map( map ) {
	
		// vars
		var bounds = new google.maps.LatLngBounds();
	
		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
	
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
	
			bounds.extend( latlng );
	
		});
	
		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}
	
	}
	
	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	// global var
	var map = null;
	
	$(document).ready(function(){
	
		$('.acf-map').each(function(){
	
			// create map
			map = new_map( $(this) );
	
		});
	
	});
	
	})(jQuery);
	
	
	$( document ).ready(function(){
		
		//Leer Valores
		var base_url = $('#base_url').val();
	
		$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
		    FB.init({
		    	appId: '1594923460547132',
				version: 'v2.11' // or v2.1, v2.2, v2.3, ...
		    });
		});
		
		//Carrousel Full TitanioTV
		$('.carousel.carousel-slider').carousel({fullWidth: true});
		
		//Habilitar Menu Mobile
		$(".button-collapse").sideNav();
	
		//Slider Home
		$('.slider').slider();
		
		$('.banner_titanio').slick({
		    infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 4000,
	    });
		
		//Materialize Select
		$('select').material_select();
		
		//Slider btn-back
		$('.slider .btn-back').on('click', function(e) {
			e.preventDefault();
			
			$('.slider').slider('prev');
			
			return false;
		});
		
		//Slider btn-forward
		$('.slider .btn-forward').on('click', function(e) {
			e.preventDefault();
			
			$('.slider').slider('next');
			
			return false;
		});
		
		//btnShareFacebook Click
	    $('.btn-facebook-sinlge').on('click', function(e) {
		    e.preventDefault();
		    var url = $(this).attr('rel');
			
			FB.ui({
				method: 'share',
				href: url,
				hashtag: '',
				mobile_iframe: true,
			}, function(response){});

	        return false;
	    });
	    
	    //btnCotizarServicio Click
	    $('#btnCotizarServicio').on('click', function(e) {
		    e.preventDefault();
		    
		    //Leemos los Datos
		    var name = $.trim($('#name').val());
		    var email = $.trim($('#email').val());
		    var phone = $.trim($('#phone').val());
		    var service = $.trim($('#service').val());
		    var comment = $.trim($('#comment').val());
		    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var regex_phone = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
			var parametros = '{"msg": "sendBudget","fields": {"name": "' + name + '","email":"' + email + '","phone":"' + phone + '","service":"' + service + '","comment":"' + comment + '"}}';
			
			//Disable Button
			$('#btnCotizarServicio').prop( 'disabled', true );
			$('#btnCotizarServicio').html('PROCESANDO...');
			
			//Verificamos que estén todos los campos
			if (name && email && phone && service && comment)
			{
				//Validate Email
				if (regex.test(email))
				{
					//Validate Phone
					if (regex_phone.test(phone))
					{
						//Request API
						$.post(base_url + '/api', { param: parametros }).done(function( data ) {
							
							//Check Result
							if (data.status == 1)
							{
								$('#name').val('');
								$('#email').val('');
								$('#phone').val('');
								$('#service').val('');
								$('#comment').val('');
								
								//Error
								Materialize.toast('Hemos recibido tu información. Te contactaremos.', 10000);
							}
							else
							{
								//Show Error
								Materialize.toast(data.msg, 4000);
							}
							
						});
					}
					else
					{
						//Error
						Materialize.toast('Escribe un teléfono válido de 10 dígitos.', 4000);
					}
				}
				else
				{
					//Error
					Materialize.toast('Escribe un correo electrónico válido.', 4000);
				}
			}
			else
			{
				//Error
				Materialize.toast('Todos los campos son obligatorios.', 4000);	
			}
			
			//Disable Button
			$('#btnCotizarServicio').prop( 'disabled', false );
			$('#btnCotizarServicio').html('COTIZAR');
		    
		    return false;
	    });
	    
	    //btnSendContact Click
	    $('#btnSendContact').on('click', function(e) {
		    e.preventDefault();
		    
		    //Leemos los Datos
		    var name = $.trim($('#name').val());
		    var email = $.trim($('#email').val());
		    var phone = $.trim($('#phone').val());
		    var comment = $.trim($('#comment').val());
		    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var regex_phone = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
			var parametros = '{"msg": "sendContact","fields": {"name": "' + name + '","email":"' + email + '","phone":"' + phone + '","comment":"' + comment + '"}}';
			
			//Disable Button
			$('#btnSendContact').prop( 'disabled', true );
			$('#btnSendContact').html('PROCESANDO...');
			
			//Verificamos que estén todos los campos
			if (name && email && phone && comment)
			{
				//Validate Email
				if (regex.test(email))
				{
					//Validate Phone
					if (regex_phone.test(phone))
					{
						//Request API
						$.post(base_url + '/api', { param: parametros }).done(function( data ) {
							
							//Check Result
							if (data.status == 1)
							{
								$('#name').val('');
								$('#email').val('');
								$('#phone').val('');
								$('#comment').val('');
								
								//Error
								Materialize.toast('Hemos recibido tu información. Te contactaremos.', 10000);
							}
							else
							{
								//Show Error
								Materialize.toast(data.msg, 4000);
							}
							
						});
					}
					else
					{
						//Error
						Materialize.toast('Escribe un teléfono válido de 10 dígitos.', 4000);
					}
				}
				else
				{
					//Error
					Materialize.toast('Escribe un correo electrónico válido.', 4000);
				}
			}
			else
			{
				//Error
				Materialize.toast('Todos los campos son obligatorios.', 4000);	
			}
			
			//Disable Button
			$('#btnSendContact').prop( 'disabled', false );
			$('#btnSendContact').html('ENVIAR');
		    
		    return false;
	    });
	    
	    //inputCatProducto Change
	    $('#inputCatProducto').on('change', function(e) {
			e.preventDefault();
		    
		    var slug = $(this).val();
		    window.location.href = base_url + '/categoria/' + slug;
		    
		    return false;
	    });
	    
	    //inputInicialGrupo Change
	    $('#inputInicialGrupo').on('change', function(e) {
			e.preventDefault();
		    
		    var slug = $(this).val();
		    window.location.href = base_url + '/inicial/' + slug;
		    
		    return false;
	    });
	    
	    //searchNav Click
	    $('#searchNav').on('click', function(e) {
		    e.preventDefault();
		    
		    $( ".nav-search" ).toggle();
		    
		    return false;
	    });
		
	});