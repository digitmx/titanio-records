		<!--Footer-->
		<div class="container-fluid" style="background: #000;">
			<div class="row">
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<div class="col s12 m12 l6 right" style="position: relative;">
						<div class="space40"></div>
						<div class="contenedor-media-footer valign-wrapper">
							<h3 class="bold font16 white-text mobile-text-center">SÍGUENOS</h3>
							<div class="contenedor-media-img valign-wrapper mobile-text-center">
								<?php if (get_field("url_facebook","option")) { ?>
								<a href="<?php the_field("url_facebook","option"); ?>" target="_blank"><img class="btn-facebook" src="<?php bloginfo('template_directory'); ?>/img/fb.svg"></a>
								<?php } ?>
								<?php if (get_field("url_instagram","option")) { ?>
								<a href="<?php the_field("url_instagram","option"); ?>" target="_blank"><img class="btn-instagram" src="<?php bloginfo('template_directory'); ?>/img/in.svg"></a>
								<?php } ?>
								<?php if (get_field("url_youtube","option")) { ?>
								<a href="<?php the_field("url_youtube","option"); ?>" target="_blank"><img class="btn-youtube" src="<?php bloginfo('template_directory'); ?>/img/yt.svg"></a>
								<?php } ?>
								<?php if (get_field("url_spotify","option")) { ?>
								<a href="<?php the_field("url_spotify","option"); ?>" target="_blank"><img class="btn-spotify" src="<?php bloginfo('template_directory'); ?>/img/sp.svg"></a>
								<?php } ?>
								<?php if (get_field("url_newsletter","option")) { ?>
								<div class="newsletter-icon">
									<a href="<?php the_field("url_newsletter","option"); ?>" target="_blank"><i class="fa fa-envelope fa-lg white-text" aria-hidden="true"></i></a>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="space40 hide-on-med-and-down"></div>
					</div>
					<div class="col s12 m12 l6 left">
						<div class="space40"></div>
						<h3 class="font16 white-text valign-wrapper mobile-text-center text-rights">
							<?php the_field("copyright_footer","option"); ?>
						</h3>
						<div class="space40 hide-on-small-only"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s4 offset-s4 m2 offset-m5 l2 offset-l5">
					<div class="centered">
						<span class="white-text">Creado por:</span>
						<div class="space20"></div>
						<a href="https://pulquedigital.com" target="_blank">
							<img class="responsive-img" src="<?php bloginfo("template_directory"); ?>/img/pd-logo.svg">
						</a>
						<div class="space40"></div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		
		<?php wp_footer(); ?>

	</body>
</html>