<?php get_header(); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>
		<?php $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>

		<div class="container-fluid white">
			<div class="row">
				<div class="space20"></div>
				<div class="col s12 m10 offset-m1 l10 offset-l1 mobile-padding-0">
					<div class="contenedor-img-single">
						<img class="responsive-img-full-w-h" src="<?php echo $image; ?>">
					</div>
				</div>
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<div class="space40"></div>
					<h3 class="bold font34 centered uppercase"><?php the_title(); ?></h3>
					<div class="space40"></div>
					<h4 class="helvetica light font18 centered uppercase"><?php the_field("subtitle", $post->ID); ?></h4>
					<div class="space40"></div>
					<p class="light font16 centered mobile-text-justify" style="max-width: 850px;">
						<?php the_content(); ?>	
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s12">
					<h3 class="bold font18 centered uppercase">COMPARTIR</h3>
					<div class="space20"></div>
					<div class="contenedor-face block centered">
						<a href="#" class="bold font20 white-text uppercase btn-facebook-sinlge inline" rel="<?php the_permalink($post->ID); ?>">facebook</a>
					</div>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		
		<!--Contacto Formulario-->
		<div class="container-fluid" style="background-color: #FFF;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<div class="space40 hide-on-small-only"></div>
					<h3 class="bold font48 gris mobile-text-center uppercase tablet-font24">Datos de Contacto</h3>
				</div>
				<div class="col s12 m8 offset-m2 l8 offset-l2">
					<form id="formCotizar" name="formCotizar" class="col s12 formulario-editar" style="padding: 0;">
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Nombre" id="name" name="name" type="text" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Correo Electrónico" id="email" name="email" type="email" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="* Teléfono" id="phone" name="phone" type="tel" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12" style="padding: 0; margin: 0;">
								<input placeholder="Servicio" disabled="disabled" id="service" name="service" type="text" value="<?php echo $post->post_title; ?>" class="edicion-input-form validate browser-default">
							</div>
						</div>
						<div class="row">
							<form class="col s12" style="padding: 0;">
								<div class="row">
									<div class="input-field col s12" style="padding: 0; margin: 0;">
										<textarea placeholder="Comentarios..." id="comment" name="comment" class="edicion-textarea-form materialize-textarea"></textarea>
									</div>
								</div>
							</form>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="btn-enviar centered">
									<button type="submit" id="btnCotizarServicio" name="btnCotizarServicio" class="btn-enviar-formulario browser-default gris bold font20 uppercase">Cotizar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?php endwhile; ?>
		<?php endif; ?>
		
		<?php 
			
			//Consultamos las noticias
			$args = array(
				'post_type'		   => 'noticia',
				'posts_per_page'   => 3,
				'order'			   => 'date',
				'orderby'          => 'DESC',
				'post_status'      => 'publish',
				'suppress_filters' => false 
			);
			$posts_array = new WP_Query( $args ); 
				
		?>

		<?php if (count($posts_array->posts) > 0) { ?>
		<div class="container-fluid" style="background-color: #E8E8E8;">
			<div class="row" style="margin-bottom: 0; padding-bottom: 40px;">
				<div class="col s12 m10 offset-m1 l10 offset-l1">
					<div class="space40"></div>
					<h3 class="bold font34 gris uppercase mobile-text-center">más noticias</h3>
					<div class="space40"></div>
				</div>
				<div class="col s12 m10 offset-m1 l10 offset-l1 no-padding">
					<?php foreach ($posts_array->posts as $noticia) { $cover = get_the_post_thumbnail_url( $noticia->ID, $size = 'full' ); ?>
					<div class="col s12 m4 l4 mobile-padding-0">
						<div class="contenedor-ficha-noticias">
							<div class="card grayscale">
								<a href="<?php echo get_permalink($noticia->ID); ?>" style="color: inherit;">
									<div class="card-image">
										<img class="responsive-img-full-w-h" src="<?php echo $cover; ?>">
									</div>
									<div class="card-content">
										<h3 class="bold font14 mobile-text-center titulo-ficha"><?php echo $noticia->post_title; ?></h3>
										<p class="font12 mobile-text-center contenido-ficha"><?php echo $noticia->post_excerpt; ?></p>
									</div>
								</a>
							</div>
						</div>
					</div>
					<?php wp_reset_postdata(); } ?>
				</div>
			</div>
		</div>
		<?php } ?>
				
<?php get_footer(); ?>