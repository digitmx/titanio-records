<?php get_header(); ?>

		<?php 
			
			global $term;	
			$categoria = get_term_by('slug', $term, 'categoria');
			
			/* Leemos los Grupos */
			$categorias = get_terms( array(
			    'taxonomy' => 'categoria',
			    'hide_empty' => false,
			) );
			
			//Consultamos las noticias
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'post_type'		   => 'producto',
				'posts_per_page'   => 9,
				'order'			   => 'date',
				'orderby'          => 'DESC',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'tax_query' => array(
					array(
						'taxonomy' => 'categoria',
						'field'    => 'term_id',
						'terms'    => $categoria->term_id,
					),
				),
				'suppress_filters' => false  
			);
			$query = new WP_Query( $args ); 
			
		?>

		<div class="container-fluid space3 noticias_bg">
			<div class="row">
				<div class="col s12 m12 l4 offset-l1">
					<div class="space40"></div>
					<h3 class="bold font48 gris uppercase mobile-text-center">catálogo</h3>
				</div>
				<div class="col s6 offset-s3 m6 offset-m3 l2">
					<div class="space40 hide-on-small-only"></div>
					<select class="browser-default roboto font18" id="inputCatProducto">
						<option value="" disabled="" selected="">Filtro de Categoría</option>
						<?php foreach ($categorias as $row) { ?>
						<option value="<?php echo $row->slug; ?>"<?=($row->slug==$categoria->slug) ? ' selected' : ''; ?>><?php echo $row->name; ?></option>
						<?php } ?>
					</select>
					<div class="space40"></div>
				</div>
				<?php if (count($query->posts) > 0) { ?>
				<div class="col s12 m10 offset-m1 l10 offset-l1 no-padding">
					<?php while ( $query->have_posts() ) : $query->the_post(); $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>
					<div class="col s12 m4 l4 mobile-padding-0">
						<div class="contenedor-ficha-noticias">
							<div class="card grayscale">
								<a href="<?php echo get_permalink($post->ID); ?>" style="color: inherit;">
									<div class="card-image">
										<img class="responsive-img-full-w-h" src="<?php echo $image; ?>">
									</div>
									<div class="card-content">
										<h3 class="bold font14 mobile-text-center titulo-ficha"><?php echo $post->post_title; ?></h3>
										<p class="font12 mobile-text-center contenido-ficha"><?php echo $post->post_excerpt; ?></p>
									</div>
								</a>
							</div>
						</div>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<div class="col s12 m12 l12 centered">
						<div class="space20"></div>
						<?php previous_posts_link( '&nbsp;' ); ?>
						<?php next_posts_link( '&nbsp;', $query->max_num_pages ); ?>
			        	<div class="space20"></div>
					</div>
				</div>
				<?php } else { ?>
				<div class="col s12 m10 offset-m1 l10 offset-l1 no-padding">
					<div class="col s12 m4 l4 mobile-padding-0">
						<div class="contenedor-ficha-noticias">
							<h3 class="bold font14 mobile-text-center">No hay productos creados aún.</h3>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

<?php get_footer(); ?>